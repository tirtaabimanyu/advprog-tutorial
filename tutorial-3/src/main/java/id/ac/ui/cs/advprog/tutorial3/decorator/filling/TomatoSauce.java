package id.ac.ui.cs.advprog.tutorial3.decorator.filling;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

public class TomatoSauce extends Food {
    Food food;

    public TomatoSauce(Food food) {
        this.food = food;
        this.description = "Tomato Sauce";
    }

    @Override
    public String getDescription() {
        return this.food.getDescription() + ", adding " + this.description.toLowerCase();
    }

    @Override
    public double cost() {
        return 0.2 + food.cost();
    }
}
