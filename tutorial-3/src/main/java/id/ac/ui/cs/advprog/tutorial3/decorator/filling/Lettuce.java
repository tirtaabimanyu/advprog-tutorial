package id.ac.ui.cs.advprog.tutorial3.decorator.filling;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

public class Lettuce extends Food {
    Food food;

    public Lettuce(Food food) {
        this.food = food;
        this.description = "Lettuce";
    }

    @Override
    public String getDescription() {
        return this.food.getDescription() + ", adding " + this.description.toLowerCase();
    }

    @Override
    public double cost() {
        return 0.75 + food.cost();
    }
}
