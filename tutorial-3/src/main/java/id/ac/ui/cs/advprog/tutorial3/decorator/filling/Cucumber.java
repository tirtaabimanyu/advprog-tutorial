package id.ac.ui.cs.advprog.tutorial3.decorator.filling;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

public class Cucumber extends Food {
    Food food;

    public Cucumber(Food food) {
        this.food = food;
        this.description = "Cucumber";
    }

    @Override
    public String getDescription() {
        return this.food.getDescription() + ", adding " + this.description.toLowerCase();
    }

    @Override
    public double cost() {
        return 0.4 + food.cost();
    }
}
