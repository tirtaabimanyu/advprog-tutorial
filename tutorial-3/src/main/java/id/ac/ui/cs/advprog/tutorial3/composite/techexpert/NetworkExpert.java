package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;
import jdk.nashorn.internal.runtime.regexp.joni.exception.ValueException;

public class NetworkExpert extends Employees {
    public NetworkExpert(String name, double salary) {
        this.name = name;
        this.salary = salary;
        this.role = "Network Expert";

        if (salary < 50000) {
          throw new IllegalArgumentException();
        }
    }

    @Override
    public double getSalary() {
        return this.salary;
    }
}
